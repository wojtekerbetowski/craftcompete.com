---
layout: index
---
## Cancelled!

This edition is cancelled and will **not happen on Saturday, 23 Sep 2017**.
Contact the organizer in order to be informed about future events.

## Welcome!

This is a community event for
[Software Craftsmen](https://en.wikipedia.org/wiki/Software_craftsmanship),
who want to validate their skills and learn something from others. Enjoy!
:tada:

## Mission

Craft compete aims to challenge our knowledge by coding, competition
and interactions. It is a challenge for experienced software developers
who are constantly questioning their own assumptions
about creating software.

## Competition

The event is a competition, so be sure to come rested and charged!

### Main category

*But how to test Software Craftsmen?* - everyone asks.
There will be coding. There will be thinking. But there will be no judging.
You will be scored totally automatically, although your score
will depend on how well others can work on your code!

Since you will collaborate with others, languages support needs to be
limited. Therefore only JavaScript, Python and Java is being allowed
during this edition.

### Side category

You will be working on your own code only, therefore you may choose
any language you want. You will be given a list of acceptance criteria
to implement. But not all of them from the beginning.

Actually most of them will be released as you go, some might change.
The better you can reuse already written code - the better you'll do.

## Organizers

Created by [Wojtek Erbetowski](https://erbetowski.pl) :wave:
with awesome volunteers support!
