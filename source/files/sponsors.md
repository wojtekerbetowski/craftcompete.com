# Craft Compete

## Sponsorship opportunities

---

# Mission

Craft compete aims to challenge our knowledge by coding, competition and interactions. It is a challenge for experienced software developers who are constantly questioning their own assumptions about creating software.


---

# Attendees

The event is created for most experienced software developers, who focus on software quality. The name - Craft Compete - refers to Software Craftsmanship movement. The event will host 80 attendees.

---

## Serial organizer

### Wojtek Erbetowski has a proven track record
### Some of his events were:

---

Warsjawa, Git kata, Name Collision, MCE, Mobile Warsaw, WJUG

---

# Sponsoring opportunities

---

# Main prize

You get a chance to present your company just before handing in the main prizes to winners!

---

# Food and drinks

You have a chance to present your company during the lunch break.

---

# Party

You have a chance to present your company and welcome attendees to the afterparty.

---

# Supporter

You have a chance to give back to the community with your contribution.

---

# Also, as a sponsor of any package ...

---

* Your logo will be displayed on our website
* You will be mentioned during the event
* You may also bring your branded gifts for attendees.
* You get 2 tickets for the event.
* Your rollup will be displayed during the event.
* You will be our hero!

---

# Pricing

---

| Package | Price (PLN + VAT) |
| --- | --- |
| Main prize | 5000 |
| Lunch and drinks | 5000 |
| Party | 5000 |
| Supporter | 1500 |
